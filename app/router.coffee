`import Ember from 'ember'`
`import config from './config/environment'`

Router = Ember.Router.extend
  location: config.locationType

Router.map () ->
  @resource 'commands'
  @resource 'logs'
  @resource 'raws'

`export default Router`
