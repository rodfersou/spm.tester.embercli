`import DS from 'ember-data'`

Device = DS.Model.extend
  connection: DS.attr('number')
  ip        : DS.attr('string')
  port      : DS.attr('number')

`export default Device`
