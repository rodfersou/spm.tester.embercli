`import DS from 'ember-data'`

Log = DS.Model.extend
  log         : DS.attr('string')
  profile     : DS.attr('string')
  origin      : DS.attr('string')
  payload     : DS.attr()
  device      : DS.attr('string')
  datetime    : DS.attr('string')
  crc         : DS.attr()
  connection  : DS.attr('string')
  last_crc    : DS.attr()
  message_type: DS.attr('string')
  log_type    : DS.attr('string')

`export default Log`
