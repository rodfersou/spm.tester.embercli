import Ember from 'ember';
import Resolver from 'ember/resolver';
import loadInitializers from 'ember/load-initializers';
import config from './config/environment';

var App;

Ember.MODEL_FACTORY_INJECTIONS = true;

Ember.LinkView.reopen({
    init: function() {
        this._super();
        var self = this;
        Em.keys(this).forEach(function(key) {
            if (key.substr(0, 5) === 'data-') {
                self.get('attributeBindings').pushObject(key);
            }
        });
    }
});

App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver: Resolver
});

loadInitializers(App, config.modulePrefix);

export default App;
