.form-group
  label.col-md-4.control-label for="device" Device
  .col-md-8
    select#device.form-control name="device"
      each device in devices
        option value=device.id {{device.id}}.{{device.connection}} - {{device.ip}}:{{device.port}}

tester-command name="Polling" action="polling"
tester-command name="Request ZWave Sensor Table" action="requestzwavesensortable"
tester-command name="Add Sensor" action="addsensor"
tester-command name="Cancel Add Sensor" action="canceladdsensor"
tester-command name="Delete Sensor" action="deletesensor"
tester-command name="Cancel Delete Sensor" action="canceldeletesensor"
tester-command name="ZWave Data" action="zwavedata" input_text="Payload"
tester-command name="Request ZWave Sensor Info" action="requestzwavesensorinfo" input_text="Sensor ID"
tester-command name="Request 433 Sensor Table" action="request433sensortable"
tester-command name="Set KeepAlive Interval" action="setkeepaliveinterval" input_text="Seconds"
tester-command name="Request Firmware Version" action="requestfirmwareversion"
tester-command name="Delete 433 Sensor" action="delete433sensor" input_text="Index"
tester-command name="Set Log" action="setlog" input_text="1-Enable 0-Disable"
tester-command name="Set Serial Number" action="setserialnumber" input_text="Serial Number"

.form-group
  label.col-md-4.control-label for="firmware-update" Firmware Update
  .col-md-8
    .input-group
      form.form-control.input-md enctype="multipart/form-data"
        input#firmware-update type="file"
      .input-group-btn.grow
        button.btn.btn-default type="button" click="firmwareupdate"
          span.glyphicon.glyphicon-upload

