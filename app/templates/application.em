nav.navbar.navbar-default
  .container-fluid
    .navbar-header
      button.navbar-toggle.collapsed type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"
        span.sr-only Toggle navigation
        span.icon-bar
        span.icon-bar
        span.icon-bar
      linkTo 'commands' class="navbar-brand" | SPM Tester
    #bs-example-navbar-collapse-1.collapse.navbar-collapse
      ul.nav.navbar-nav
        active-link
          linkTo 'commands' | Commands
        active-link
          linkTo 'logs' | Log
        active-link
          linkTo 'raws' | Raw

.container-fluid
  = outlet
