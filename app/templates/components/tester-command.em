.form-group
  label.col-md-4.control-label for="command" = name
  .col-md-8
    .input-group
      if has_input
        Ember.TextField class="form-control input-md" valueBinding=data name="address" type="text" placeholder=input_text
        .input-group-btn
          button.btn.btn-default type="button" click="'send' data"
            span.glyphicon.glyphicon-play
      else
        .input-group-btn.grow
          button.btn.btn-default type="button" click="send"
            span.glyphicon.glyphicon-play
