`import DS from 'ember-data'`

ApplicationAdapter = DS.RESTAdapter.extend
  host: (() ->
    a = document.createElement('a')
    a.href = location.href
    a.port = 5001
    a.pathname = ''
    a.search = ''
    a.hash = ''
    return a.href.replace(/\/$/, '')
  ).property().volatile()
  pathForType: (type) ->
    return Ember.String.underscore(type)

`export default ApplicationAdapter`
