`import Ember from 'ember'`

TesterCommandComponent = Ember.Component.extend
  init: () ->
    @_super()
    @restApiBaseUrl = @_controller.restApiBaseUrl
    @set('has_input', @get('input_text')?)

  actions:
    send: (param='') ->
      device = $('#device').val()
      if not device?
        alert('Conecte um controlador')
        return

      page = "#{@restApiBaseUrl}#{@get('action')}/#{device}"
      if param != ''
        page += "/#{param}"

      $.ajax(
          url    : page
          method : 'POST'
          context: @
      ).done (data) ->
        #debugger

`export default TesterCommandComponent`
