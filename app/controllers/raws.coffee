`import Ember from 'ember'`

RawsController = Ember.Controller.extend
  init: () ->
    @_super()
    tick = () =>
      @update()
      Ember.run.later(tick, 1000)
    tick()

  update: () ->
    @store.findAll('raw', reload: true)

`export default RawsController`
