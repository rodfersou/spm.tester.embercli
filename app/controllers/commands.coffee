`import Ember from 'ember'`

CommandsController = Ember.Controller.extend
  restApiPort: 5001
  restApiBaseUrl: ''
  devices: []

  init: () ->
    @_super()
    a = document.createElement('a')
    a.href = location.href
    a.port = @restApiPort
    a.pathname = ''
    a.search = ''
    a.hash = ''
    @set('restApiBaseUrl', a.href)
    @set('devices', @store.findAll('device'))

    tick = () =>
      @update()
      Ember.run.later(tick, 5000)
    tick()

  update: () ->
    @store.unloadAll('device')
    @store.findAll('device', reload: true)

  actions:
    firmwareupdate: () ->
      firmware_update = $('#firmware-update')[0]
      formData = new FormData()
      formData.append('file', firmware_update.files[0]);

      xhr = new XMLHttpRequest()
      url = "#{@restApiBaseUrl}firmwareupdate/#{$('#device').val()}"
      xhr.open('POST', url, true)
      xhr.send(formData)  # multipart/form-data

`export default CommandsController`
