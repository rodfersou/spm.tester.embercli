`import Ember from 'ember'`

LogsController = Ember.Controller.extend
  init: () ->
    @_super()
    @set('logs', @store.findAll('log'))

    tick = () =>
      @update()
      Ember.run.later(tick, 1000)
    tick()

  update: () ->
    @store.findAll('log', reload: true)

`export default LogsController`
