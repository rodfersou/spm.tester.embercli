`import Ember from 'ember'`

IndexRoute = Ember.Route.extend
  beforeModel: () ->
    @transitionTo('commands')

`export default IndexRoute`
