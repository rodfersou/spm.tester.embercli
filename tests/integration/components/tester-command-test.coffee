`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'tester-command', 'Integration | Component | tester command', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{tester-command}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#tester-command}}
      template block text
    {{/tester-command}}
  """

  assert.equal @$().text().trim(), 'template block text'
